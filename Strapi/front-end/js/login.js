$(document).ready(function(){
    $("#username").focus(function(){
        $("#username").attr({"placeholder": ""});
    });
    $("#username").blur(function(){
        $("#username").attr({"placeholder": "Username"});
    });
    $("#password").focus(function(){
        $("#password").attr({"placeholder": ""});
    });
    $("#password").blur(function(){
        $("#password").attr({"placeholder": "Password"});
    });
})