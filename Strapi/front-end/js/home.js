$(document).ready(function () {
  $.ajax({
    type: "GET",
    dataType: 'json',
    url: 'http://localhost:1337/motels/'
  })
    .done(function (data) {
      let counter = 0;
      for (row in data) {
        $("table tbody").append("<tr><th scope='row'>" + (counter + 1) + "</th>" +
          "<td><a href='https://www.w3schools.com'>" + data[counter].name + "</a></td>" +
          "<td>" + data[counter].desciption + "</td>" +
          "<td>" + data[counter].name + "</td></tr>"
        );
        counter++;
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      // If fail
      $("table tbody").append("<tr><td colspan='4' style='text-align: center'>" + "Sorry, Cannot connect to server!" + "</td></tr>");
    });
});